using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;

namespace Ordertaker
{
    public partial class Debug : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //HttpContext.Current.Request.UserHostAddress;
            //HttpContext.Current.Request.Browser.Type;
            //HttpContext.Current.User.Identity.Name;
            //HttpContext.Current.Server.MachineName;
            //HttpContext.Current.Request.LogonUserIdentity.Name;
            string sub, sub2, sub3, sub4, sub5 = "";
            sub = HttpContext.Current.Request.UserHostAddress.ToString();
            sub2 = HttpContext.Current.Request.Browser.Type.ToString();
            sub3 = HttpContext.Current.User.Identity.Name.ToString();
            sub4 = HttpContext.Current.Server.MachineName.ToString();
            sub5 = HttpContext.Current.Request.LogonUserIdentity.Name.ToString();
            Label1.Text = "Hi " + sub + " : Request.UserHostAddress";
            Label2.Text = "Hi " + sub2 + " : Request.Browser.Type";
            Label3.Text = "Hi " + sub3 + " : User.Identity.Name";
            Label4.Text = "Hi " + sub4 + " : Server.MachineName";
            Label5.Text = "Hi " + sub5 + " : Request.LogonUserIdentity.Name";

            //force to https
            if (!Request.IsLocal && !string.Equals(Request.Headers["X-Forwarded-Proto"], "https", StringComparison.InvariantCultureIgnoreCase))
            {
                string redirectUrl = Request.Url.ToString().Replace("http:", "https:");
                string redirectUrlFixedPort = Regex.Replace(redirectUrl, @".com(.*)/", @".com:443/");
                Response.Redirect(redirectUrlFixedPort, false);
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
        }
    }
}
