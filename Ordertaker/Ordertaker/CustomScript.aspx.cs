﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;

namespace Ordertaker
{
    public partial class CustomScript : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //force to https
            if (!Request.IsLocal && !string.Equals(Request.Headers["X-Forwarded-Proto"], "https", StringComparison.InvariantCultureIgnoreCase))
            {
                string redirectUrl = Request.Url.ToString().Replace("http:", "https:");
                string redirectUrlFixedPort = Regex.Replace(redirectUrl, @".com(.*)/", @".com:443/");
                Response.Redirect(redirectUrlFixedPort, false);
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
        }

        protected void ButtonExecute_Click(object sender, EventArgs e)
        {
            SqlConnection myConnection;
            if (HttpContext.Current.Request.IsLocal)
            {
                //myConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["localLPDConnectionString"].ConnectionString);
                if (System.Environment.MachineName.Equals("LPD-0250"))
                {
                    myConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["localLPDConnectionString"].ConnectionString);
                }
                else if (System.Environment.MachineName.Equals("P245"))
                {
                    myConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["localP245ConnectionString"].ConnectionString);
                }
                else
                {
                    throw new Exception("Unknown computer name");
                }
            }
            else
            {
                myConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["dbf7181123fd1f4cf9890aa40a0036b873ConnectionString"].ConnectionString);
            }
            
            myConnection.Open();
            SqlCommand myCommand = myConnection.CreateCommand();
            myCommand.CommandText = TextBoxQuery.Text;
            myCommand.CommandType = System.Data.CommandType.Text;

            try
            {
                if (TextBoxQuery.Text.Contains("SELECT"))
                {
                    //SqlDataReader myDataReader = myCommand.ExecuteReader();
                    //while (myDataReader.Read())
                    //{

                    //}

                    SqlDataAdapter myDataAdapter = new SqlDataAdapter(myCommand);
                    DataSet myDataSet = new DataSet();
                    myDataAdapter.Fill(myDataSet);
                    GridView1.DataSource = myDataSet;
                    GridView1.DataBind();
                }
                else
                {
                    int rowsAffected = myCommand.ExecuteNonQuery();
                    LabelRowsAffected.Text = "Rows affected: " + rowsAffected.ToString();
                }
            }
            catch (SqlException sex)
            {
                LabelRowsAffected.Text = sex.ToString();

            }
            finally
            {
                if (myConnection != null && myConnection.State == ConnectionState.Open)
                {
                    myConnection.Close();
                }
            }
        }

    }
}