﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CustomScript.aspx.cs" Inherits="Ordertaker.CustomScript" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
                    <asp:LinkButton ID="LinkButton1" runat="server" 
                        PostBackUrl="~/Order.aspx?myvariable=etmagboo" CausesValidation="False">Back</asp:LinkButton>
        <br />
    
        Enter Custom Script:<br />
        <asp:TextBox ID="TextBoxQuery" runat="server" Height="102px" TextMode="MultiLine" Width="312px"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidatorQuery" runat="server" ControlToValidate="TextBoxQuery" ErrorMessage="Custom script cannot be empty"></asp:RequiredFieldValidator>
        <br />
        <br />
        <asp:Button ID="ButtonExecute" runat="server" OnClick="ButtonExecute_Click" Text="Execute" />
&nbsp;&nbsp;&nbsp;
        <asp:Label ID="LabelRowsAffected" runat="server" Text="Rows affected:"></asp:Label>
        <br />
        <asp:GridView ID="GridView1" runat="server">
            <EmptyDataTemplate>
                Empty Data Template Displayed
            </EmptyDataTemplate>
        </asp:GridView>
    
    </div>
    </form>
</body>
</html>
