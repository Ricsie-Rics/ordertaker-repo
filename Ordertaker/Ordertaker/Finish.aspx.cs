using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;

namespace Ordertaker
{
    public partial class Finish : System.Web.UI.Page
    {
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (Request.QueryString["myvariable"] != null)
            {
                LabelThankYou.Text = Request.QueryString["myvariable"].ToString();
            }

            //Request.QueryString["myvariable"].ToString()
         }

        protected void Page_Load(object sender, EventArgs e)
        {
            //LabelThankYou.Text = Request["LabelGreeting"].ToString();
            // string myInputText = ((TextBox)PreviousPage.FindControl("txtMyInput")).Text;
            //if(!IsPostBack)
            //    LabelThankYou.Text = ((Label)PreviousPage.FindControl("LabelGreeting")).Text;

            //force to https
            if (!Request.IsLocal && !string.Equals(Request.Headers["X-Forwarded-Proto"], "https", StringComparison.InvariantCultureIgnoreCase))
            {
                string redirectUrl = Request.Url.ToString().Replace("http:", "https:");
                string redirectUrlFixedPort = Regex.Replace(redirectUrl, @".com(.*)/", @".com:443/");
                Response.Redirect(redirectUrlFixedPort, false);
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
        }


        protected void LinkButtonLogout_Click(object sender, EventArgs e)
        {
            FormsAuthentication.SignOut();
            //Roles.DeleteCookie();
            Session.Clear();
            Response.Redirect("Default.aspx");
        }

    }
}
