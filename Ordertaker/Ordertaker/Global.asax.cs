using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace Ordertaker
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            Application.Add("UserCount", 0);
        }

        protected void Application_End(object sender, EventArgs e)
        {

        }

        protected void Session_Start(object sender, EventArgs e)
        {
            int userCount = int.Parse(Application.Get("UserCount").ToString());
            userCount++;

            Application.Set("userCount", userCount);
        }


        protected void Session_End(object sender, EventArgs e)
        {
            int userCount = int.Parse(Application.Get("UserCount").ToString());
            userCount--;

            Application.Set("userCount", userCount);
        }
    }
}