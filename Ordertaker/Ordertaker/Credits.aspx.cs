using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;

namespace Ordertaker
{
    public partial class Credits : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //force to https
            if (!Request.IsLocal && !string.Equals(Request.Headers["X-Forwarded-Proto"], "https", StringComparison.InvariantCultureIgnoreCase))
            {
                string redirectUrl = Request.Url.ToString().Replace("http:", "https:");
                string redirectUrlFixedPort = Regex.Replace(redirectUrl, @".com(.*)/", @".com:443/");
                Response.Redirect(redirectUrlFixedPort, false);
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
        }
    }
}
