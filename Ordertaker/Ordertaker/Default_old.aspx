<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Ordertaker._Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Home Page</title>
</head>
<body>
    <form id="form1" runat="server" defaultfocus="TextBoxUsername" defaultbutton="ButtonLogin">
    <div>
        <span style="text-decoration: underline"><strong>
            <asp:Label ID="LabelCurrentUsers" runat="server" Font-Bold="False" Width="462px"></asp:Label><br />
        WELCOME!</strong><br />
        </span>
        <br />
        Ikaw ba ay wala pang lunch?<br />
        Kung oo, tara na 't umorder dito sa Ordertaker Website :)<br />
        Syempre, mag log-in ka muna...<br />
        <br />
        <table>
            <tr>
                <td style="width: 26px; height: 12px;">
                </td>
                <td style="width: 205px; height: 12px;">
                    <asp:Label ID="Label1" runat="server" Text="Username:" Width="200px"></asp:Label>&nbsp;</td>
                <td style="width: 190px; height: 12px;">
                </td>
                <td style="width: 190px; height: 12px">
                    Global Orders for Today: <%# getdate() %> </td>
            </tr>
            <tr>
                <td style="width: 26px; height: 29px;">
                </td>
                <td style="width: 205px; height: 29px;">
                    <asp:TextBox ID="TextBoxUsername" runat="server" Width="200px"></asp:TextBox></td>
                <td style="width: 190px; height: 29px;">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBoxUsername"
                        ErrorMessage="Required po"></asp:RequiredFieldValidator></td>
                <td rowspan="5" style="width: 190px">
                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" Width="427px">
                        <Columns>
                            <asp:BoundField DataField="orderUser" HeaderText="orderUser" SortExpression="orderUser" />
                            <asp:BoundField DataField="orderName" HeaderText="orderName" SortExpression="orderName" />
                            <asp:BoundField DataField="orderQuantity" HeaderText="orderQuantity" SortExpression="orderQuantity" >
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="orderPrice" HeaderText="orderPrice" SortExpression="orderPrice" >
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td style="width: 26px; height: 21px">
                </td>
                <td style="width: 205px; height: 21px">
                    <asp:Label ID="Label2" runat="server" Text="Password:" Width="200px"></asp:Label></td>
                <td style="width: 190px; height: 21px">
                </td>
            </tr>
            <tr>
                <td style="width: 26px; height: 20px;">
                </td>
                <td style="width: 205px; height: 20px;">
                    <asp:TextBox ID="TextBoxPassword" runat="server" Width="200px" TextMode="Password"></asp:TextBox></td>
                <td style="width: 190px; height: 20px">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextBoxPassword"
                        ErrorMessage="Required po"></asp:RequiredFieldValidator></td>
            </tr>
            <tr>
                <td style="width: 26px; height: 31px;">
                </td>
                <td style="width: 205px; height: 31px;">
                    <asp:Button ID="ButtonLogin" runat="server" OnClick="ButtonLogin_Click1" Text="Login" /></td>
                <td style="width: 190px; height: 31px">
                </td>
            </tr>
            <tr>
                <td style="width: 26px; height: 18px">
                </td>
                <td style="width: 205px; height: 18px">
                    <asp:Label ID="LabelLoginError" runat="server" Width="199px" ForeColor="Red"></asp:Label></td>
                <td style="width: 190px; height: 18px">
                </td>
            </tr>
        </table>
        <br />
        <br />
        <br />
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="Data Source=LPD-0098;Initial Catalog=ordertaker;Persist Security Info=True;User ID=testdb;Password=qweasd"
                        ProviderName="System.Data.SqlClient" SelectCommand="sp_getglobalorders" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>
        <br />
        </div>
    </form>
</body>
</html>
