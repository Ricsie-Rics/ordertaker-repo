using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace Ordertaker
{
    public partial class Admin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //force to https
            if (!Request.IsLocal && !string.Equals(Request.Headers["X-Forwarded-Proto"], "https", StringComparison.InvariantCultureIgnoreCase))
            {
                string redirectUrl = Request.Url.ToString().Replace("http:", "https:");
                string redirectUrlFixedPort = Regex.Replace(redirectUrl, @".com(.*)/", @".com:443/");
                Response.Redirect(redirectUrlFixedPort, false);
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }

            //setting the  connection string of SQLDataSource1
            //
            if (HttpContext.Current.Request.IsLocal)
            {
                if (System.Environment.MachineName.Equals("LPD-0250"))
                {
                    SqlDataSource1.ConnectionString = ConfigurationManager.ConnectionStrings["localLPDConnectionString"].ConnectionString;
                }
                else if (System.Environment.MachineName.Equals("P245"))
                {
                    SqlDataSource1.ConnectionString = ConfigurationManager.ConnectionStrings["localP245ConnectionString"].ConnectionString;
                }
                else
                {
                    throw new Exception("Unknown computer name");
                }
            }
            else
            {
                SqlDataSource1.ConnectionString = ConfigurationManager.ConnectionStrings["dbf7181123fd1f4cf9890aa40a0036b873ConnectionString"].ConnectionString;
            }

            if (!IsPostBack)
            {
                //SqlConnection mycon = new SqlConnection("Data Source=LPD-0250;Initial Catalog=ordertaker;Persist Security Info=True;User ID=sa;Password=p@ssw0rd;");
                //SqlConnection mycon = new SqlConnection(ConfigurationManager.ConnectionStrings["dbf7181123fd1f4cf9890aa40a0036b873ConnectionString"].ConnectionString);
                SqlConnection mycon; 
                if (HttpContext.Current.Request.IsLocal)
                {
                    if (System.Environment.MachineName.Equals("LPD-0250"))
                    {
                        mycon = new SqlConnection(ConfigurationManager.ConnectionStrings["localLPDConnectionString"].ConnectionString);
                    }
                    else if (System.Environment.MachineName.Equals("P245"))
                    {
                        mycon = new SqlConnection(ConfigurationManager.ConnectionStrings["localP245ConnectionString"].ConnectionString);
                    }
                    else
                    {
                        throw new Exception("Unknown computer name");
                    }
                }
                else
                {
                    mycon = new SqlConnection(ConfigurationManager.ConnectionStrings["dbf7181123fd1f4cf9890aa40a0036b873ConnectionString"].ConnectionString);
                }
                
                mycon.Open();
                SqlCommand mycommand = mycon.CreateCommand();
                mycommand.CommandText = "SELECT dishId, dishName, dishPrice FROM dish";
                mycommand.CommandType = CommandType.Text;
                SqlDataReader mydatareader;
                mydatareader = mycommand.ExecuteReader();

               
                mycon.Close();

                TextBoxUsername.Text = "etmagboo";
                TextBoxUsername.Visible = false;
            }

            //test


        }

        protected void ButtonSet_Click(object sender, EventArgs e)
        {
            string str = string.Empty;
            string strname = string.Empty;
            //List<menuclass> Lmenuclass = new List<menuclass>();
            foreach (GridViewRow gvrow in GridView1.Rows)
            {
                CheckBox chk = (CheckBox)gvrow.FindControl("CheckBox1");
                if (chk != null && chk.Checked)
                {
                    str += GridView1.DataKeys[(gvrow.RowIndex)].Value.ToString() + ',';
                    strname += gvrow.Cells[2].Text + ',';
                    //menuclass menu = new menuclass();
                    //menu.dishId = Convert.ToInt32(GridView1.DataKeys[(gvrow.RowIndex)].Value);
                    //menu.menuDate = DateTime.Today;
                    //Lmenuclass.Add(menu);

                    //SqlConnection mycon = new SqlConnection("Data Source=LPD-0250;Initial Catalog=ordertaker;Persist Security Info=True;User ID=sa;Password=p@ssw0rd;");
                    //SqlConnection mycon = new SqlConnection(ConfigurationManager.ConnectionStrings["dbf7181123fd1f4cf9890aa40a0036b873ConnectionString"].ConnectionString);
                    SqlConnection mycon;
                    if (HttpContext.Current.Request.IsLocal)
                    {
                        if (System.Environment.MachineName.Equals("LPD-0250"))
                        {
                            mycon = new SqlConnection(ConfigurationManager.ConnectionStrings["localLPDConnectionString"].ConnectionString);
                        }
                        else if (System.Environment.MachineName.Equals("P245"))
                        {
                            mycon = new SqlConnection(ConfigurationManager.ConnectionStrings["localP245ConnectionString"].ConnectionString);
                        }
                        else
                        {
                            throw new Exception("Unknown computer name");
                        }
                    }
                    else
                    {
                        mycon = new SqlConnection(ConfigurationManager.ConnectionStrings["dbf7181123fd1f4cf9890aa40a0036b873ConnectionString"].ConnectionString);
                    }
                    mycon.Open();
                    SqlCommand mycommand = mycon.CreateCommand();
                    mycommand.CommandText = "sp_populatemenu";
                    mycommand.CommandType = CommandType.StoredProcedure;
                    mycommand.Parameters.AddWithValue("@dishId", Convert.ToInt32(GridView1.DataKeys[(gvrow.RowIndex)].Value));
                    mycommand.ExecuteNonQuery();
                    mycon.Close();
                }

            }
            str = str.Trim(",".ToCharArray());
            strname = strname.Trim(",".ToCharArray());
            Label1.Text = "Selected UserIds: <b>" + str + "</b><br/>" + "Selected UserNames: <b>" + strname + "</b>";
        }

        protected void ButtonReset_Click(object sender, EventArgs e)
        {
            //SqlConnection mycon = new SqlConnection("Data Source=LPD-0250;Initial Catalog=ordertaker;Persist Security Info=True;User ID=sa;Password=p@ssw0rd;");
            SqlConnection mycon;
            if (HttpContext.Current.Request.IsLocal)
            {
                if (System.Environment.MachineName.Equals("LPD-0250"))
                {
                    mycon = new SqlConnection(ConfigurationManager.ConnectionStrings["localLPDConnectionString"].ConnectionString);
                }
                else if (System.Environment.MachineName.Equals("P245"))
                {
                    mycon = new SqlConnection(ConfigurationManager.ConnectionStrings["localP245ConnectionString"].ConnectionString);
                }
                else
                {
                    throw new Exception("Unknown computer name");
                }
            }
            else
            {
                mycon = new SqlConnection(ConfigurationManager.ConnectionStrings["dbf7181123fd1f4cf9890aa40a0036b873ConnectionString"].ConnectionString);
            }
            mycon.Open();
            SqlCommand mycommand = mycon.CreateCommand();
            mycommand.CommandText = "sp_resetmenufortheday";
            mycommand.CommandType = CommandType.StoredProcedure;
            mycommand.ExecuteNonQuery();
            mycon.Close();
        }

        //protected void CheckBox1_CheckedChanged(object sender, EventArgs e)
        //{
        //nagchange kasi ng template
        //}

        protected void CheckBox1_CheckedChanged1(object sender, EventArgs e)
        {
            foreach (GridViewRow myrow in GridView1.Rows)
            {
                CheckBox chkbx = (CheckBox)myrow.FindControl("CheckBox1");
                if (chkbx != null && chkbx.Checked)
                {
                    //GridView1.SelectedRowStyle = myrow.RowIndex;
                    GridView1.Rows[myrow.RowIndex].BackColor = System.Drawing.Color.Yellow;
                }
                else
                {
                    GridView1.Rows[myrow.RowIndex].BackColor = System.Drawing.Color.Transparent;
                }
            }
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            //string targetURL;
            //targetURL = "Order.aspx?";
            //targetURL += "myvariable=";
            //targetURL += TextBoxUsername.Text;
            //Response.Redirect(targetURL);
        }


    }
}
