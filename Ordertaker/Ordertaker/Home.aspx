<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="Ordertaker.Home" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Home Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <span style="text-decoration: underline"><strong>
        WELCOME!</strong><br />
        </span>
        <br />
        Ikaw ba ay wala pang lunch?<br />
        <br />
        Kung oo, tara na 't umorder dito sa Ordertaker Website :)<br />
        <br />
        <br />
        Syempre, mag log-in ka muna...<br />
        <br />
        <table>
            <tr>
                <td style="width: 26px">
                </td>
                <td style="width: 205px">
                    <asp:Label ID="Label1" runat="server" Text="Username:" Width="200px"></asp:Label>&nbsp;</td>
                <td style="width: 205px">
                </td>
            </tr>
            <tr>
                <td style="width: 26px">
                </td>
                <td style="width: 205px">
                    <asp:TextBox ID="TextBoxUsername" runat="server" Width="200px"></asp:TextBox></td>
                <td style="width: 205px">
                    <asp:RequiredFieldValidator ID="userNameRequiredFieldValidator1" runat="server" ControlToValidate="TextBoxUsername"
                        ErrorMessage="*Username Required" Enabled="False"></asp:RequiredFieldValidator></td>
            </tr>
            <tr>
                <td style="width: 26px; height: 21px">
                </td>
                <td style="width: 205px; height: 21px">
                    <asp:Label ID="Label2" runat="server" Text="Password:" Width="200px"></asp:Label></td>
                <td style="width: 205px; height: 21px">
                </td>
            </tr>
            <tr>
                <td style="width: 26px; height: 21px;">
                </td>
                <td style="width: 205px; height: 21px;">
                    <asp:TextBox ID="TextBoxPassword" runat="server" Width="200px" TextMode="Password"></asp:TextBox></td>
                <td style="width: 205px; height: 21px">
                    <asp:RequiredFieldValidator ID="passwordRequiredFieldValidator1" runat="server" ControlToValidate="TextBoxPassword"
                        ErrorMessage="*Password Required" Enabled="False"></asp:RequiredFieldValidator></td>
            </tr>
            <tr>
                <td style="width: 26px; height: 31px;">
                </td>
                <td style="width: 205px; height: 31px;">
                    <asp:Button ID="ButtonLogin" runat="server" Text="Login" OnClick="ButtonLogin_Click" PostBackUrl="~/Order.aspx"  /></td>
                <td style="width: 205px; height: 31px">
                </td>
            </tr>
            <tr>
                <td style="width: 26px; height: 18px">
                </td>
                <td style="width: 205px; height: 18px">
                    <asp:Label ID="LabelLoginError" runat="server" Width="199px" ForeColor="Black"></asp:Label></td>
                <td style="width: 205px; height: 18px">
                </td>
            </tr>
        </table>
    
    </div>
        <asp:Button ID="Button1" runat="server" PostBackUrl="~/Order.aspx" Text="Button" />
    </form>
</body>
</html>
