<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Admin.aspx.cs" Inherits="Ordertaker.Admin"  MaintainScrollPositionOnPostback="true"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Admin Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table style="z-index: 100; left: 0px; width: 428px; position: absolute; top: 0px;
            height: 1px">
            <tr>
                <td style="width: 100px; height: 21px">
                </td>
                <td style="width: 148px; height: 21px">
                </td>
                <td style="width: 90441px; height: 21px">
                </td>
            </tr>
            <tr>
                <td style="width: 100px">
                </td>
                <td style="width: 148px">
                    <asp:GridView ID="GridView1" runat="server" Height="41px" Width="321px" AutoGenerateColumns="False" DataKeyNames="dishId" DataSourceID="SqlDataSource1" AllowSorting="True">
                        <Columns>
                            <asp:BoundField DataField="dishId" HeaderText="dishId" InsertVisible="False" ReadOnly="True"
                                SortExpression="dishId" />
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:CheckBox ID="CheckBox1" runat="server" AutoPostBack="True" OnCheckedChanged="CheckBox1_CheckedChanged1" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="dishName" HeaderText="dishName" SortExpression="dishName" />
                            <asp:BoundField DataField="dishPrice" HeaderText="dishPrice" SortExpression="dishPrice" />
                        </Columns>
                    </asp:GridView>
                </td>
                <td style="width: 90441px">
                </td>
            </tr>
            <tr>
                <td style="width: 100px; height: 9px">
                </td>
                <td style="width: 148px; height: 9px">
                    <asp:Button ID="ButtonSet" runat="server" OnClick="ButtonSet_Click" Text="Set" Width="41px" />
                    <asp:Button ID="ButtonReset" runat="server" OnClick="ButtonReset_Click" Text="Reset" />
                    <asp:Label ID="Label1" runat="server" Text="Label" Width="250px"></asp:Label></td>
                <td style="width: 90441px; height: 9px">
                    <asp:LinkButton ID="LinkButton1" runat="server" PostBackUrl="~/Order.aspx?myvariable=etmagboo" OnClick="LinkButton1_Click">Back</asp:LinkButton></td>
            </tr>
        </table>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="Data Source=LPD-0250;Initial Catalog=ordertaker;Persist Security Info=True;User ID=sa;Password=p@ssw0rd"
            ProviderName="System.Data.SqlClient" SelectCommand="SELECT * FROM [dish]" ConflictDetection="CompareAllValues" DeleteCommand="DELETE FROM [dish] WHERE [dishId] = @original_dishId AND (([dishName] = @original_dishName) OR ([dishName] IS NULL AND @original_dishName IS NULL)) AND (([dishPrice] = @original_dishPrice) OR ([dishPrice] IS NULL AND @original_dishPrice IS NULL))" InsertCommand="INSERT INTO [dish] ([dishName], [dishPrice]) VALUES (@dishName, @dishPrice)" OldValuesParameterFormatString="original_{0}" UpdateCommand="UPDATE [dish] SET [dishName] = @dishName, [dishPrice] = @dishPrice WHERE [dishId] = @original_dishId AND (([dishName] = @original_dishName) OR ([dishName] IS NULL AND @original_dishName IS NULL)) AND (([dishPrice] = @original_dishPrice) OR ([dishPrice] IS NULL AND @original_dishPrice IS NULL))">
            <DeleteParameters>
                <asp:Parameter Name="original_dishId" Type="Int32" />
                <asp:Parameter Name="original_dishName" Type="String" />
                <asp:Parameter Name="original_dishPrice" Type="String" />
            </DeleteParameters>
            <UpdateParameters>
                <asp:Parameter Name="dishName" Type="String" />
                <asp:Parameter Name="dishPrice" Type="String" />
                <asp:Parameter Name="original_dishId" Type="Int32" />
                <asp:Parameter Name="original_dishName" Type="String" />
                <asp:Parameter Name="original_dishPrice" Type="String" />
            </UpdateParameters>
            <InsertParameters>
                <asp:Parameter Name="dishName" Type="String" />
                <asp:Parameter Name="dishPrice" Type="String" />
            </InsertParameters>
        </asp:SqlDataSource>
        &nbsp;&nbsp;
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <asp:TextBox ID="TextBoxUsername" runat="server"></asp:TextBox><br />
    
    </div>
    </form>
</body>
</html>
