<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Order.aspx.cs" Inherits="Ordertaker.Order"  MaintainScrollPositionOnPostback="true"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Order Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Label ID="LabelGreeting" runat="server" Width="404px"></asp:Label><br />
        <asp:LinkButton ID="LinkButtonBack" runat="server" PostBackUrl="~/Admin.aspx">Set ulam</asp:LinkButton>
        <asp:HyperLink ID="HyperLink1" runat="server" Font-Underline="True" ForeColor="Blue"
            NavigateUrl="~/Debug.aspx" Visible="False">Debug</asp:HyperLink>
        <asp:LinkButton ID="LinkButtonLogout" runat="server" OnClick="LinkButtonLogout_Click">Logout</asp:LinkButton><br />
        <asp:Label ID="LabelOrderInfo" runat="server" Width="402px"></asp:Label><br />
        <table>
            <tr>
                <td style="width: 115px; height: 2px;">
                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1"
                        Width="399px" ShowFooter="True" FooterStyle-BackColor="LightSlateGray" Height="126px">
                        <Columns>
                            <asp:BoundField DataField="Item" HeaderText="Item" ReadOnly="True" SortExpression="Item" />
                            <asp:BoundField DataField="Price" HeaderText="Price" ReadOnly="True" SortExpression="Price" />
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:CheckBox ID="CheckBox1" runat="server" AutoPostBack="True" OnCheckedChanged="CheckBox1_CheckedChanged" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:Label ID="Labelx" runat="server" Text="x"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Quantity" FooterText="Total">
                                <ItemTemplate>
                                    <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
                                        <asp:ListItem Selected="True">1</asp:ListItem>
                                    </asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Sub Total">
                                <ItemTemplate>
                                    <asp:Label ID="LabelSubTotal" runat="server" Style="text-align: right" EnableViewState="False"></asp:Label>
                                    
                                </ItemTemplate>
                                <FooterTemplate>  </FooterTemplate>
                                <FooterStyle Font-Bold="True" ForeColor="Red" />
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="MintCream" Font-Bold="True" HorizontalAlign="Right" />
                    </asp:GridView>                    
                    <asp:Label ID="Label1" runat="server" Width="374px"></asp:Label>
                    <asp:Label ID="LabelTotal" runat="server" Text="0" Width="21px" Font-Bold="True" ForeColor="Red"></asp:Label></td>
                <td style="width: 100px; height: 2px;">
                    &nbsp;<asp:Image ID="Image1" runat="server" Height="174px" ImageUrl="~/images/ordertakerlogo.png"
                        Width="201px" /></td>
                   
            </tr>
            <tr>
                <td style="width: 115px; height: 119px;">
                    <em><strong><span style="color: #00cc66">Freebies:<br />
                    </span></strong></em>
                    <asp:CheckBoxList ID="CheckBoxList1" runat="server" Width="131px">
                        <asp:ListItem>Sabaw</asp:ListItem>
                        <asp:ListItem>Ketchup</asp:ListItem>
                        <asp:ListItem>Toyomansi</asp:ListItem>
                    </asp:CheckBoxList></td>
                <td style="width: 100px; height: 119px;">
                    </td>
            </tr>
            <tr>
                <td style="width: 115px;">
                    <asp:Button ID="ButtonSubmit" runat="server" Text="Submit" OnClick="ButtonSubmit_Click" ValidationGroup="second" />
                    <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="TextBox2"
                        ErrorMessage="Null Order!" MaximumValue="999" MinimumValue="1" Type="Integer"
                        ValidationGroup="second"></asp:RangeValidator>
                    <asp:TextBox ID="TextBox2" runat="server" BackColor="Transparent" BorderColor="Transparent"
                        ForeColor="Transparent" ReadOnly="True" ValidationGroup="second"></asp:TextBox></td>
                <td style="width: 100px;">
                    </td>
            </tr>
        </table>
        <br />
        Ordered already but want to edit?<br />
        You can start over. This will erase all your orders for today<br />
        <asp:Button ID="ButtonClearOrder" runat="server" Font-Bold="True" ForeColor="Red"
            OnClick="ButtonClearOrder_Click" Text="Clear Orders" ValidationGroup="third"
            Width="91px" />
        <asp:Label ID="LabelClearOrderSuccess" runat="server" ForeColor="#00C000" Width="256px"></asp:Label>
        <asp:TextBox ID="TextBoxOrderCount" runat="server" BackColor="Transparent" BorderColor="Transparent"
            ForeColor="Transparent" ValidationGroup="third"></asp:TextBox><br />
        &nbsp;<asp:RangeValidator ID="RangeValidator2" runat="server" ControlToValidate="TextBoxOrderCount"
            ErrorMessage="There is nothing to clear!" MaximumValue="99" MinimumValue="1"
            Type="Integer" ValidationGroup="third"></asp:RangeValidator><br />
        <asp:LinkButton ID="LinkButtonCredits" runat="server" CausesValidation="False" PostBackUrl="~/Credits.aspx">Credits</asp:LinkButton><br />
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="Data Source=LPD-0250;Initial Catalog=ordertaker;Persist Security Info=True;User ID=sa;Password=p@ssw0rd"
            ProviderName="System.Data.SqlClient" SelectCommand="sp_getmenufortheday" SelectCommandType="StoredProcedure">
        </asp:SqlDataSource>
        </div>
    </form>
</body>
</html>
