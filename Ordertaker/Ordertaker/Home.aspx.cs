using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Ordertaker
{
    public partial class Home : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //ButtonLogin.PostBackUrl = "Order.aspx";
            if (!IsPostBack)
                Page.Response.Write("Current online users: " + Application.Get("UserCount").ToString());
        }

        protected void ButtonLogin_Click(object sender, EventArgs e)
        {
            string[,] array1 = { { "SN",    "User Name",    "Password" },
                                 { "1",     "etmagboo",     "130280" },
                                 { "2",     "bsvalencia",   "bergs"},
                                 { "3",     "kaesguerra",   "kim"},
                                 { "4",     "jddelacruz",   "aldwin"}
                               };
            bool isAuthorize = false;
            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (TextBoxUsername.Text == array1[i, j])
                        if (TextBoxPassword.Text == array1[i, j + 1])
                        {
                            isAuthorize = true;
                        }
                        else
                        {

                        }
                }
            }

            if (isAuthorize == true)
            {
                LabelLoginError.ForeColor = System.Drawing.Color.Green;
                LabelLoginError.Text = "LoginSucces!";

            }
            else
            {
                LabelLoginError.ForeColor = System.Drawing.Color.Red;
                LabelLoginError.Text = "Wrong Login!";
            }
        }
    }
}
